# MMKV

## 介绍

一款小型键值对存储框架

- 支持存储 number、boolean、string、Set<String>类型数据存储
- 支持继承组件中 SerializeBase.ets 的 class 类对象的序列化反序列化
- 支持存储数据备份
- 支持存储数据恢复

## 下载安装

```
ohpm install @ohos/mmkv
```
OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)


## 使用

### 1、初始化：设置 mmkv 保存文件根目录(rootPath)和缓存目录(cachePath)

    MMKV.initialize(rootPath, cachePath)

### 2、实例化 mmkv：

    let mmkv = MMKV.getBackedUpMMKVWithID(mmapID, MMKV.SINGLE_PROCESS_MODE, "Tencent MMKV", backupRootDir);

### 3、存取键值对数据:
#### 3.1 常用数据类型：boolean、number、string、Set<string>
##### 存数据示例：

    mmkv.encodeBool('abool', false)
    mmkv.encodeNumber('anumber', 3.0122)
    mmkv.encodeString('astring', 'dsfsg')
    let set1 = new Set<string>()
    set1.add('ass1')
    mmkv.encodeSet('aSet', set1)
##### 取数据示例：

    mmkv.decodeBool('abool')
    mmkv.decodeNumber('astring')
    mmkv.decodeString('aNumber')
    mmkv.decodeSet('aSet')

#### 3.2 类对象数据的序列化反序列化
##### 类对象需要继承 SerializeBase类，需序列化属性需要标识注解@Serialize() 如：

    class MyClass extends SerializeBase{
        @Serialize()
        public code: number = 0;
        public title: string = 'titles';
        @Serialize()
        public description: string = 'descs';
    }
##### 存数据：

    let myClass1 = new MyClass(1, 't1', 'desc1')
    kv.encodeSerialize('serialize111', myClass1)
##### 取数据：

    let myClass2 = kv.decodeSerialize('serialize111', new MyClass())

### 4、系统轻量级存储数据转存为mmkv存储

* API9及以上适用

    mmkv.preferenceToMMKV(name)  //name:preference文件名

### 5、设置加密密钥

    mmkv.reCryptKey('Key_seq_1') //Key_seq_1：加密密钥

### 6、数据备份

##### 备份otherDir路径mmapID的mmkv存储数据到backupRootDir

    MMKV.backupOneToDirectory(mmapID, backupRootDir, otherDir)//mmapID：需要备份的mmapID；backupRootDir：备份到目标路径；otherDir：待备份所在路径

##### 备份全部mmkv存储数据到backupRootDir

    MMKV.backupAllToDirectory(backupRootDir) //backupRootDir：备份到目标路径

### 7、数据恢复

##### 从srcDir恢复mmkv存储数据

    MMKV.restoreOneMMKVFromDirectory(mmapID, srcDir, otherDir)//mmapID：需要恢复的mmapID；srcDir：目标路径；otherDir：待备份所在路径

##### 恢复srcDir路径下的全部mmkv存储数据

    MMKV.restoreAllFromDirectory(srcDir) //srcDir： 目标路径

### 8、清除所有存储数据

    mmkv.clearAll()

## 约束与限制

在下述版本验证通过：

DevEco Studio: 3.1 Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)

## 目录结构

```
|----MMMKV
|     |---- entry  # 示例代码文件夹
|     |---- MMMKV  # MMMKV库文件夹
|           |---- index.ets  # 对外接口
|     |---- README.md  # 安装使用方法
```

## 贡献代码

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-tpc/MMKV/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-tpc/MMKV/pulls) 。

## 开源协议

本项目基于 [BSD 3-Clause License](https://www.tizen.org/bsd-3-clause-license) ，请自由地享受和参与开源。
